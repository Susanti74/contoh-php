<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Array</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        echo "<h4>Kids Group</h4>";
        $Kids = ["Mike", "Dustin", "Lucas", "Max", "Eleven"];
        $Adults = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];
        print_r($Kids);
        echo "<h4>Adults Group</h4>";
        print_r($Adults);
        


        echo "<h3>Soal 2</h3>";

        echo "Cast Stranger Things : " . "<br>";
       
        echo "Total Kids " . count($Kids);
        echo "<ol>";
        echo "<li>" . $Kids[0] . "</li>";  
        echo "<li>" . $Kids[1] . "</li>";
        echo "<li>" . $Kids[2] . "</li>";
        echo "<li>" . $Kids[3] . "</li>";
        echo "<li>" . $Kids[4] . "</li>";
        echo "</ol>";

        echo "Total Adults" . count($Adults);
        echo "<ol>";
        echo "<li>" . $Adults[0] . "</li>";
        echo "<li>" . $Adults[1] . "</li>";
        echo "<li>" . $Adults[2] . "</li>";
        echo "<li>" . $Adults[3] . "</li>";
        echo "<li>" . $Adults[4] . "</li>";
        echo "</ol>"; 




        echo "<h3>Soal No 3</h3>";
        $Status = [
            ["Name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
            ["Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["Name" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
            ["Name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"],
        ];

        echo "<pre>";
        print_r($Status);
        echo "</pre>"; 
    ?>

</body>
</html>